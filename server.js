const express = require('express');
const faker = require('faker');
const cors = require('cors');
const app = express();

function generateUsers(count) {
  const users = [];
  const userProfiles = {};

  for (let i = 0; i < count; i++) {
    let id = faker.random.uuid();
    let username = faker.internet.userName();

    users.push({ id, username });

    userProfiles[id] = {
      id,
      username,
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      address: faker.address.streetAddress(),
      county: faker.address.county(),
      country: faker.address.country(),
      postcode: faker.address.zipCode()
    };
  }

  return { users, userProfiles };
}

const { users, userProfiles } = generateUsers(100);
const port = 3000;
const failureRate = 30;

app.use(cors());
app.use(express.json());
app.use((req, res, next) => {
  if (Math.floor(Math.random() * 100) <= failureRate) return res.send(500);

  return next();
})

app.get('/users', (req, res) => res.send(users));

app.get('/users/:userId', (req, res) => {
  const { userId } = req.params;
  const user = userProfiles[userId];

  if (!user) return res.status(400).send('Invalid User ID');

  return res.send(userProfiles[userId]);
});

app.post('/users/:userId', (req, res) => {
  const { userId } = req.params;

  if (!userProfiles[userId]) return res.status(400).send('Invalid User ID');
  if (!req.body) return res.status(400).send('Empty request body')

  const requiredKeys = [
    'firstName',
    'lastName',
    'email',
    'address',
    'county',
    'country',
    'postcode'
  ]

  const missingKeys = [];
  const updatedUser = {};
  // validate user
  requiredKeys.forEach(key => {
    if (!req.body[key]) return missingKeys.push(key)

    updatedUser[key] = req.body[key];
  })

  if (missingKeys.length > 0) return res.status(400).send(`Missing required keys: ${missingKeys.join(', ')}`)

  // write user
  Object.keys(updatedUser).forEach(key => {
    userProfiles[userId][key] = updatedUser[key];
  })

  res.send(userProfiles[userId]);
})

app.listen(port, () => {
  console.log('Server listening on port:', port);
});
