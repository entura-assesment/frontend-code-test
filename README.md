# Frontend Code Test - User List
## Requirements
Given the following API create a react/redux application which meets the following requirements:
- display a list of all usernames and IDs, sorted alphanumerically.
- allows selection of a users profile - the the selected users profile should then be displayed
- allows the following fields of the users profile to be edited & saved (the UI must reflect any changes made):
```text
firstName
lastName
email
address
county
country
```
- The API will respond with a 500 HTTP status 30% of the time - the react application should handle this as gracefully as possible.
- Feel free to use any libraries/modules/tooling which you feel will improve the quality/readability of your code and/or help you get the job done more quickly!


The completed test should aim to display a good understanding of:
- React/Redux
- unit/integration tests
- Responsive styling
- Intuitive UX/Design
- Error handling
- ES6/ES7
- Consistent code style & best practices
- project structure
- Documentation

Note: If you are unable to meet all the requirements don't worry - include a short explaination of how you'd plan to approach them.

## Users API
### Running the express API
```
npm install
npm start
```

### API Schema
The server exposes the following API on port 3000, all requests & responses are `application/json`. Feel free to improve any of the route handlers or add more features if you wish (don't remove the 30% failure middleware though!)

#### `GET /users`
Reponds with a list of usernames & IDs

Response Body:
```json
[
    {
        "id": "edcd82f0-8024-4913-b59a-de6d3006837d",
        "username": "Romaine_Langworth57"
    },
    ...
]
```

#### `GET /users/:userId`
Responds with the user profile for `userId`

Response Body:
```json
{
    "id": "edcd82f0-8024-4913-b59a-de6d3006837d",
    "username": "Romaine_Langworth57",
    "firstName": "Stewart",
    "lastName": "Goyette",
    "email": "Theresia94@gmail.com",
    "address": "9975 Kutch Street",
    "county": "Bedfordshire",
    "country": "Bermuda",
    "postcode": "92128-8241"
}
```

#### `POST /users/:userId`
Update the user profile for `userId`

Request Body:
```json
{
    "firstName": "John",
    "lastName": "Doe",
    "email": "John@Doe.com",
    "address": "9975 A Street",
    "county": "Some County",
    "country": "Some Country",
    "postcode": "12345-1234"
}
```

Response Body:
```json
{
    "id": "edcd82f0-8024-4913-b59a-de6d3006837d",
    "username": "Romaine_Langworth57",
    "firstName": "John",
    "lastName": "Doe",
    "email": "John@Doe.com",
    "address": "9975 A Street",
    "county": "Some County",
    "country": "Some Country",
    "postcode": "12345-1234"
}
```

Note: All keys are required when making this request
